/*
 * 有1000瓶水，3个瓶盖可以再换1瓶，问可以喝多少瓶水？
 */
public class TestDrinkWater {
	public static void main(String[] args) {
		func1();
		func2();
		func3();
	}
	
	static void func1() {
		/*
		 * 方法1: 一有3个瓶盖，就立即再换1瓶水
		 * 
		 */
		int n = 1000; //手中水的瓶数
		int sum = 0; //已喝水的数量
		int capNum = 0; //空瓶盖数量
		
		
		while (n > 0) {
			--n;
			++sum;
			++capNum;
			if (capNum == 3) {
				capNum = 0;
				n += 1;
			}
		}
		
		System.out.println("1:喝水的瓶数=" + sum);
	}
	
	static void func2() {
		/*方法2：等水喝完了，再把手中的瓶盖换成水，再接着喝
		 * 
		 */
		int sum = 0; //已喝水的数量
		int capNum = 0; //空瓶盖数量
		int n = 1000; //还有水的数量
		
		while (n > 0) {
			while (n > 0) {
				--n;
				++sum;
				++capNum;
			}
			n = capNum / 3;
			capNum %= 3;
		}
		System.out.println("2:喝水的瓶数=" + sum);
		
	}
	
	static void func3() {
		/*
		 * 方法3：先喝完3的整数倍瓶水，再喝剩下的水和刚才瓶盖换的水,直到剩余水不到3瓶
		 * 
		 */
		int n = 1000;
		System.out.println("3:喝水的瓶数=" + g(n));
		
	}
	
	static int g(int n) {
		if (n < 3) {
			return n;
		}
		return n / 3 * 3 + g(n % 3 + n / 3);
	}
}
